<?php

class DB
{
    /**
     * @desc     Creates the MySQLi object for usage.
     * @param    <arr> $db Required connection params.
     */
    public function  __construct($db) {
        $this->mysqli = new mysqli($db['host'], $db['user'], $db['pass'], $db['database'], $db['port']);

        if (mysqli_connect_errno())
        {
            printf("<b>Connection failed:</b> %s\n", mysqli_connect_error());
            exit;
        }

        $this->mysqli->query("SET NAMES 'utf8'");
        $this->mysqli->query("SET CHARACTER SET utf8");
        $this->mysqli->query("SET COLLATION_CONNECTION = 'utf8_turkish_ci'");

    }

    /**
     * @param $sql
     * @param $values
     * @param $type
     * @return array
     */
    public function preparedSelect($sql, $values, $type)
    {
        if( !is_array($values) ){ $values = [$values]; }

        /* create a prepared statement */
        if (!$stmt = $this->mysqli->prepare($sql)) {
            echo "Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            exit;
        }
        $stmt->bind_param($type, ...$values);
        $stmt->execute();
        $result = $stmt->get_result();
        $totalSet = [];
        while( $data = $result->fetch_assoc()){
            $totalSet[] = $data;
        }
        return $totalSet;
    }

    /**
     * @param $table
     * @param $data
     * @return mixed
     * @throws Exception
     */
    public function preparedInsert($table, $data){
        $columns = array_keys($data);
        $questionmarks =  rtrim(str_repeat("?,", count($columns)), ",");
        $types = '';
        $values = [];
        foreach($data as $column =>$value){
            $types.=    $value['type'];
            $values[] = $value['value'];
        }
        if( !$stmt = $this->mysqli->prepare("INSERT INTO ".$table." (".implode(", ", $columns).") VALUES (".$questionmarks.")") ){
            throw new \Exception("Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
        }
        $stmt->bind_param($types, ...$values);
        if( !$stmt ) {
            throw new \Exception("Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
        }
        if( !$stmt->execute() ){
            throw new \Exception("Prepare failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error);
        }
        return $this->mysqli->insert_id;
    }

    /**
     * @param string $date
     * @return null
     */
    static function asDateTime(?string $date){
        if( !$date || strlen($date)===0 ){
            return null;
        }
        try{
            return new \DateTime($date);
        }catch (\Exception $e){

            if( preg_match("/\d{1,2}\/\d{1,2}\/(\d{2}|\d{4})/", $date) ){
                return \DateTime::createFromFormat("d/m/Y", $date);
            }
            if( preg_match("/\d{1,2}-\d{1,2}-(\d{2}|\d{4})/", $date) ){
                return \DateTime::createFromFormat("d-m-Y", $date);
            }
            return null;
        }
    }

    /**
     * @desc    Automatically close the connection when finished with this object.
     */
    public function __destruct()
    {
        $this->mysqli->close();
    }

}