<?php

namespace App\src;
use App\services\UserService;
use ImportType;
use pcrov\JsonReader\JsonReader;
use DB;
use Config;


class UserImporter
{
    private $src;
    private $mode;
    private $db;
    private $userService;
    private $stats;

    const USER_MIN_AGE = 18;
    const USER_MAX_AGE = 65;

    public function __construct()
    {
        $this->db = new DB(Config::$db);
        $this->userService = new UserService();
        $this->stats = [
            'hasRun' => false,
            'numEntries' => 0,
            'usersAdded' => 0,
            'addressAdded' => 0,
            'creditcardsAdded' => 0,
        ];
    }

    /**
     * Set an URI to a file and mark it as filetype $mode
     * @param string $src
     * @param string $mode
     * @throws \Exception
     */
    public function setFileUri(string $src, string $mode){
        if (!file_exists($src)) {
            throw new \Exception("Given source does not exist (Checked: '<em>$src</em>')");
        }
        $this->src = $src;
        $this->mode = $mode;
    }

    /**
     * Set an URI to a file and mark it as JSON (Shortcut function)
     * @param string $src
     * @throws \Exception
     */
    public function setJsonFileUri(string $src){
        $this->setFileUri($src, ImportType::JSON);
    }

    /**
     * Set an URI to a file and mark it as XML (Shortcut function)
     * @param string $src
     * @throws \Exception
     */
    public function setXmlFileUri(string $src){
        $this->setFileUri($src, ImportType::XML);
    }

    /**
     * Parses the source
     * @throws \pcrov\JsonReader\Exception
     * @throws \pcrov\JsonReader\InputStream\IOException
     * @throws \pcrov\JsonReader\InvalidArgumentException
     */
    public function parse(){
        if( !$this->src ){
            throw new \Exception("Source is not set, please use '".__CLASS__."::setFileUri' or a shortcut function");
        }
        switch($this->mode){
            case ImportType::JSON:
                $this->parseAsJson();
                break;
            case ImportType::XML:
                $this->parseAsXml();
                break;
            default:
                throw new \Exception("Unknown mode '<em>$this->mode</em>'");
                break;
        }
    }

    /**
     * @throws \pcrov\JsonReader\Exception
     * @throws \pcrov\JsonReader\InputStream\IOException
     * @throws \pcrov\JsonReader\InvalidArgumentException
     * @throws \Exception
     */
    private function parseAsJson()
    {
        $reader = new JsonReader();
        $reader->open( $this->src );
        $reader->read(); // Outer array.
        $depth = $reader->depth(); // Check in a moment to break when the array is done.
        $reader->read(); // Step to the first object.
        $this->stats['hasRun'] = true;
        do {
            $this->handleOneLine( $reader->value() );
        } while ($reader->next() && $reader->depth() > $depth); // Read each sibling.

        $reader->close();
    }

    /**
     *
     */
    public function parseAsXml()
    {
        // Out of the scope of the project, but placed here for demo purposed.
        // Normally I would not create this function, functions should be created when needed, no placeholders.

        // set the data after parsing it with something like simpleXML (https://www.php.net/manual/en/book.simplexml.php)
        // This function should also use the following line to store data:
        // $this->handleOneLine( $reader->value() );
    }

    /**
     * @param $values
     * @throws \Exception
     */
    private function handleOneLine($values){
        $dateOfBirth = DB::asDateTime($values['date_of_birth']);

        $minAge = (new \DateTime())->modify("-".self::USER_MIN_AGE." years");
        $maxAge = (new \DateTime())->modify("+".self::USER_MAX_AGE." years");

        if( is_null($dateOfBirth) || ($dateOfBirth > $minAge && $dateOfBirth <= $maxAge) ) {
            $this->saveToDatabase($values);
        }
    }

    /**
     * @param array $rowValues
     * @return mixed
     * @throws \Exception
     */
    private function saveToDatabase(array $rowValues){
        $user = $this->db->preparedSelect("SELECT id FROM user WHERE email=? LIMIT 1", $rowValues['email'], 's');
        if( count($user)===0 ){
            $user['id'] = $this->userService->createUser($rowValues);
            $this->stats['usersAdded']++;
        } else{
            $user = current($user); // get first value
        }

        // Proccess the address
        $address = $this->db->preparedSelect(
            "SELECT id FROM address WHERE user_id=? AND address=? LIMIT 1",
            [$user['id'], $rowValues['address']],
            'is');
        if( count($address)===0 ) {
            $this->userService->addAddress($user['id'], $rowValues);
            $this->stats['addressAdded']++;
        }
        // Proccess the CC
        $creditcard = $this->db->preparedSelect(
            "SELECT id FROM creditcard WHERE user_id=? AND number=? LIMIT 1",
            [$user['id'], $rowValues['credit_card']['number']],
            'is');
        if( count($creditcard)===0 ) {
            $this->userService->addCreditcard($user['id'], $rowValues['credit_card']);
            $this->stats['creditcardsAdded']++;
        }

        $this->stats['numEntries']++;
    }

    /**
     * @return array
     */
    public function getStats(){
        return $this->stats;
    }

}