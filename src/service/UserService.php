<?php

namespace App\services;

use Config;
use DB;

class UserService {
    private $db;

    public function __construct()
    {
        $this->db = new DB(Config::$db);
    }


    /**
     * Creates a new user in the database and returns the insert_id
     * @param array $values
     * @return mixed
     * @throws \Exception
     */
    public function createUser(array $values){
        $parsedDOB = DB::asDateTime($values['date_of_birth']);
        $insertId = $this->db->preparedInsert('user', [
            'name'          => ['type'=>'s', 'value'=> $values['name']],
            'account'       => ['type'=>'i', 'value'=> $values['account']],
            'checked'       => ['type'=>'i', 'value'=> $values['checked']==1 ? 1:0],
            'description'   => ['type'=>'s', 'value'=> $values['description']],
            'interest'      => ['type'=>'s', 'value'=> $values['interest']],
            'dateOfBirth'   => ['type'=>'s', 'value'=> !$parsedDOB ? NULL : $parsedDOB->format("Y-m-d H:i:s")],
            'email'         => ['type'=>'s', 'value'=> $values['email']],
        ]);
        return $insertId;
    }

    /**
     * @param int $userId
     * @param array $values
     * @return mixed
     * @throws \Exception
     */
    public function addAddress(int $userId, array $values){
        $insertId = $this->db->preparedInsert('address', [
            'user_id'          => ['type'=>'s', 'value'=> $userId],
            'address'          => ['type'=>'s', 'value'=> $values['address']],
        ]);
        return $insertId;
    }

    /**
     * @param int $userId
     * @param array $values
     * @throws \Exception
     */
    public function addCreditcard(int $userId, array $values){
        $expirationDate = \DateTime::createFromFormat("m/Y", $values['expirationDate']);
        $this->db->preparedInsert('creditcard', [
            'user_id'        => ['type'=>'s', 'value'=> $userId],
            'type'           => ['type'=>'s', 'value'=> $values['type']],
            'number'         => ['type'=>'s', 'value'=> $values['number']],
            'expirationDate' => ['type'=>'s', 'value'=> $expirationDate->format("d-m-Y")],
        ]);

    }
}