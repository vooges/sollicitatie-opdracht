<?php
namespace App;

require_once('config.php');
require_once('src/DB.php');
require_once('src/UserImporter.php');
require_once('src/data/ImportType.php');
require_once('src/service/UserService.php');

require __DIR__ . '/vendor/autoload.php';


use App\src\UserImporter;


////////////////////////////////////////////////////////////////
/// Ik heb ervoor gekozen om een class neer te plaatsen om de code heen, ook al voegt dat in dit voorbeeld weinig toe.
/// Ik heb in mn achterhoofd gehouden dat het in een productieomgeving zou kunnen eindigen, wat OO is, vandaar de
///     implementatie via een class
////////////////////////////////////////////////////////////////

class App {
    private $importer;

    /**
     * App constructor.
     * @throws \pcrov\JsonReader\Exception
     * @throws \pcrov\JsonReader\InputStream\IOException
     * @throws \pcrov\JsonReader\InvalidArgumentException
     * @throws \Exception
     */
    public function __construct($startImport){
        if( $startImport ) {
            $this->importer = new UserImporter();
            $this->importer->setJsonFileUri('./files/challenge.json');
            $this->importer->parse();

            echo '<pre>';
            print_r($this->importer->getStats());
        }
    }

}

$app = new App( isset($_GET['start']) );

?>
Om het importeren <?=isset($_GET['start'])?'nogmaals':''?> te starten <a href="?start">kun je hier klikken</a>.