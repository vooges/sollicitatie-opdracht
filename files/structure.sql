CREATE TABLE `address` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`address` VARCHAR(1000) NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;


CREATE TABLE `creditcard` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) NOT NULL,
	`type` ENUM('Visa','Mastercard','Discover Card','American Express') NOT NULL,
	`number` BIGINT(20) NOT NULL,
	`expirationDate` DATE NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;

CREATE TABLE `user` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`checked` TINYINT(1) NOT NULL DEFAULT '0',
	`name` VARCHAR(100) NOT NULL,
	`description` TEXT NULL DEFAULT NULL,
	`email` VARCHAR(250) NOT NULL,
	`dateOfBirth` DATETIME NULL DEFAULT NULL,
	`interest` TEXT NULL DEFAULT NULL,
	`account` BIGINT(20) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM
AUTO_INCREMENT=1
;
