#Sollicitatie opdracht - Martijn Vooges
Er is gevraagd voor mijn sollicitatie een opdracht te maken, de opdracht is te vinden in:  
*/files/SollicitatieOpdracht.pdf*

##Project draaien krijgen:
- Creëer een database en import de bijgeleverde SQL uit */files/structure.sql*
- Vul de waardes in config.php in
- Draai `composer install` in de DOC_ROOT
- Open /index.php, het script begint **niet** direct 

##Stack
Ik maak dit op m'n eigen windows-omgeving met de WAMP stack, in PHPStorm.  
- CPU: Ryzen 5 2600 (6core) (Gemiddeld verbruik tijdens tests <40%)  
- Memory: 16GB (Gemiddeld verbruik tijdens tests <1GB))
- Schijf: SSD M.2

##Onderbouwing keuzes
###Geen framework
Ik ben Symfony gewend om te gebruiken, maar Symfony doet heel veel voor je (net als Laravel). Ik heb gekozen om het zonder framework te doen omdat dat beter aangeeft wat ik zelf heb gedaan en daardoor dus een betere weergave is van mijn vaardigheid. Het project is daarom her en der een beetje ruig om de randjes. 

###Manier van starten van de import
Als je nu de index.php in je browser opent, moet je eerst op een link klikken om deze te activeren.  
Normaal zou ik dit doen door er een commando van te maken, of mogelijk via een uploadformulier of API, maar als DEMO leek me dit de makkelijkste manier om het te starten.  

Als "achtergrondtaak in Laravel" heb ik voor nu overgeslagen, gezien ik dan eerst een Laravel project moet opzetten en ik dat nog nooit heb gedaan. Ik voorzie daar niet een probleem, maar voor nu heb ik dit out-of-scope beschouwd.

###Streaming JSON parser:
Het bestand is al redelijk groot om soepel mee te werken, en bij de bonus wordt al aangegeven dat het bestand in de toekomst groter zou kunnen worden.
Omdat we nu toch nieuw beginnen en het verschil tussen `json_decode` en deze library gebruiken betreft implementatietijd weinig verschilt, heb ik het direct ingebouwd.

###Database class
Ik heb van het internet een database class voor PHP afgehaald, maar deze eigenlijk weer gestript.  
Ik heb er zelf de prepared statements ingezet omdat deze er niet in stonden en ik standaard niet vertrouw wat er in een JSON bestand staat (zeker indien aangeleverd door een externe partij).
Ook de `asDateTime` is van mij, zodat er een functie globaal beschikbaar is om een datum te parsen welke de DB in kan worden geplaatst.

###Database structuur
Ik heb gekozen voor de tabellen `user`, `address` en `creditcard`. Op deze manier is er direct rekening gehouden met meerdere adressen en creditcards. Aangezien er in de aangeleverde dataset meer users zijn dan adressen/creditcards is dat ook in deze dataset al van toepassing.  

Veel van de columns zijn `NOT NULL`. Ik verwacht dat je alle gegevens invuld. Op deze manier worden andere programmeurs "geforceerd" te zorgen dat de data zo netjes mogelijk in de database eindigt. Hoe meer er NULL mag zijn, hoe meer controles je moet gaan inbouwen in alle andere code. Mocht je toch iets als NULL wilt kunnen doen is afwijken een bewuste keuze i.p.v. "omdat het kon".
  

###Exceptions i.p.v. dingen afvangen:
Ik ben van mening dat dingen die niet fout horen te gaan, duidelijk fout moeten gaan. De eindgebruiker krijgt uiteraard nette meldingen, maar tijdens het ontwikkelen en gebruik van de functionaliteit geef ik de voorkeur aan *full-stop on error*. Naar mijn ervaring resulteert zo veel mogelijk doorgaan alleen maar tot scheve data en/of aparte quircks. En in sommige gevallen kom je de error dan nooit tegen omdat t toevallig *goed genoeg* gaat en dan heb je je incorrecte code in je productieomgeving voordat je daar achter komt.

#NiceToHave's
#### Global error catching
Op dit moment krijg je gewoon de exception handling die je eigen ontwikkelomgeving heeft te zien. Normaal zou ik een algemene catch functionaliteit maken met een manier om de developers op de hoogte te stellen van de error (bv via email, of een library zoals Sentry.io)

####Create Constraints / auto/cascade delete
De tabellen zijn op dit punt gewoon manueel aangemaakt. Ik ben de Symfony objectmanager gewent waar ik automatisch contrains opgeef zodat je niet bijv. een `user` kunt verwijderen dat nog een `address` heeft, of juist dat als je de `user` verwijderd, de bijbehorende `address` ook automatisch delete.
  
Aangezien ik het vanaf 0 zelf heb gemaakt gebeurd dat niet uit zichzelf. Indien dit een groter/echt project had geweest, had ik zelf de constrains toegevoegd zodat de data netjes blijft.

####Commandline versie met feedback
Momenteel activeer je de functionaliteit via je browser. Dit soort functionaliteit zou ook heel goed via de commandline kunnen, een API, een formulier of bijvoorbeeld een cronjob die een mapje uitleest.

#Bonus
#### Wat nu als het bronbestand ineens 500 keer zo groot wordt?
De huidige code maakt gebruikt van een stream inlezen i.p.v. alles in 1x de buffer inladen. De code zou dus al een aanzienlijke dataset aan moeten kunnen, het duurt alleen langer.  
Nu werkt het voor de demo via de browser, maar bij zo'n grote set moet het via een command zodat je niet last krijg van timeouts en ongetwijfeld gaat er het een en ander moeten worden gebenchmarkt. 	

#### Is het proces makkelijk in te zetten voor een XML- of CSV-bestand met vergelijkbare content?
Ja. In de  `UserImporter` kun je momenteel alleen JSON importeren via `UserImporter::parseAsJson`, maar het werkend maken van `UserImporter::parseAsXml` zou redelijk eenvoudig moeten gaan.
Beide functies maken gebruik van `UserImporter::handleOneLine` voor elke regel dat ze doorlopen. Als je zorgt dat *welk-format-dan-ook* de data juist in die functie stopt, verwerkt het script het verder.

#### Stel dat alleen records moeten	worden verwerkt waarvoor geldt dat het creditcardnummer drie opeenvolgende zelfde cijfers bevat, hoe zou je dat aanpakken?	
Momenteel wordt er enkel een controle gedaan op leeftijd in `UserImporter::handleOneLine`, maar dit zou makkelijk kunnen worden uitgebreid met meer controles. Je kunt eigen/meer functies toevoegen. Let op: Indien je meerdere controles gaat maken, is het verstandig om de snelste tests eerst te doen, in balans met de functies die het meeste kunnen uitsluiten.
   
Snel voorbeeld van StackOverflow: https://stackoverflow.com/questions/37540800/determine-if-a-string-contains-a-sequence-of-numbers
