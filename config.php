<?php

class Config
{
    static $db = [
        'host' => 'localhost',
        'user'  => 'root',
        'pass'  => '',
        'database' => 'sollicitatie',
        'port' => '3307',
    ];
}